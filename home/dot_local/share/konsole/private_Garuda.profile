[Appearance]
ColorScheme=Sweet
Font=Source Code Pro,12,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[Cursor Options]
CursorShape=2
CustomCursorColor=255,0,0
UseCustomCursorColor=true

[General]
Command=/bin/fish
Name=Garuda
Parent=FALLBACK/

[Interaction Options]
AutoCopySelectedText=true
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true
