#!/bin/bash
EXT=.mkv
mkdir -p converted
for f in *$EXT; do
NAME=$(echo $f | sed "s/$EXT//g")
ffmpeg -i "$f" -map 0:0 -map 0:1 -map 0:2 -vcodec copy -acodec pcm_s24le -f mov converted/"$NAME".mov
done
